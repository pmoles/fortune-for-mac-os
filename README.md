# Random quotes in your terminal

### SETUP:
Replace _{YOUR_MASHAPE_KEY_FOR_ANDRUXNET_API}_ in [constants.py](/constants.py) with your [API Key from Mashape]("https://market.mashape.com/andruxnet/random-famous-quotes").

### USAGE:
It can either be used as standalone or every time a terminal is opened*:
```
python %PATH_TO_THE_SCRIPT%/fortune.py
```
*Edit in the Preferences panel of your preferred shell app in order to run this command above at the beginning of the 
session, potentially preceded by command _clean_ so the command itself it's not visible.