# Author: Pablo Moles
# Read the README.md file for more info

from random import random
from urllib2 import URLError, HTTPError

from unirest import post, timeout

from constants import HEADER_APP_JSON, HEADER_FORM_URLENCODED, HEADER_MASHAPE_API_KEY, MASHAPE_FAMOUS_QUOTES_URL
from dataaccessobject import DataAccessObject
from quote import Quote

class Fortune():

	def getQuoteFromRequest(self):
		timeout(0.5)
		response = post(MASHAPE_FAMOUS_QUOTES_URL,
		                headers={
			                "X-Mashape-Key": HEADER_MASHAPE_API_KEY,
			                "Content-Type": HEADER_FORM_URLENCODED,
			                "Accept": HEADER_APP_JSON
		                }
		                )
		self.handleResponse(response)

	def handleResponse(self, response):
		self.quote = Quote(response.body["quote"],
		                   response.body["author"],
		                   response.body["category"].lower() == "famous")

	def saveQuote(self):
		with DataAccessObject() as dao:
			dao.save(self.quote)

	def loadRandomQuote(self):
		with DataAccessObject() as dao:
			quoteList = dao.load()["quotes"]
			randomQuoteIdx = random.randint(0, len(quoteList) - 1)
			self.quote = Quote(quoteList[randomQuoteIdx]["quote"],
		                  quoteList[randomQuoteIdx]["author"],
		                  quoteList[randomQuoteIdx]["famous"])

	def setApologyQuote(self):
		self.quote = Quote("This is embarassing, I could not get a quote :(", "Pabs, the script creator", True)
	def execute(self):
		try:
			self.getQuoteFromRequest()
			self.saveQuote()
		except URLError:
			self.loadRandomQuote()
		except HTTPError:
			self.loadRandomQuote()
		except Exception:
			self.setApologyQuote()
		finally:
			print str(self.quote)

if __name__ == "__main__":
	Fortune().execute()
