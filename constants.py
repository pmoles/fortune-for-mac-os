MASHAPE_FAMOUS_QUOTES_URL = "https://andruxnet-random-famous-quotes.p.mashape.com/cat=famous"
HEADER_APP_JSON = "application/json"
HEADER_FORM_URLENCODED = "application/x-www-form-urlencoded"
HEADER_MASHAPE_API_KEY = {YOUR_MASHAPE_KEY_FOR_ANDRUXNET_API}

INIT_TERM_YELLOW_COLOR = "\033[93m"
END_TERM_COLOR = "\033[0m"

QUOTES_FILE_NAME = "quotes.json"
QUOTES_MAX_STORAGE = 20