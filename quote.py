from constants import END_TERM_COLOR, INIT_TERM_YELLOW_COLOR

class Quote():
	def __init__(self, txt, auth, famous):
		self.quote = txt
		self.author = auth
		self.famous = famous

	def __str__(self):
		quote = self.quote + '\n'
		if self.famous:
			quote += " -- "
		else:
			quote += " From "
		quote += self.author
		return INIT_TERM_YELLOW_COLOR + quote + END_TERM_COLOR

	def toJson(self):
		return {'author':self.author, 'quote':self.quote, 'famous':self.famous}