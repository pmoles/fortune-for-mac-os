import simplejson

from constants import QUOTES_FILE_NAME, QUOTES_MAX_STORAGE

class DataAccessObject():
	def __init__(self):
		self.filename = QUOTES_FILE_NAME
		self.maxQuotes = QUOTES_MAX_STORAGE

	def load(self):
		with open(self.filename, 'r') as jsondb:
			return simplejson.load(jsondb)

	def save(self, quote):
		rawJson = self.load()
		if len(rawJson["quotes"]) <= self.maxQuotes:
			rawJson["quotes"].append(quote.toJson())
			with open(self.filename, 'w') as jsondb:
				simplejson.dump(rawJson, jsondb)

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback ):
		return True